import { Link } from "react-router-dom";

const items = [
  {
    link: "/",
    name: "Home",
    id: 1,
  },
  {
    link: "/sensors",
    name: "Sensors",
    id: 2,
  },
  {
    link: "/map",
    name: "Map",
    id: 3,
  },
  {
    link: "/reports",
    name: "Reports",
    id: 4,
  },
];

const Navigation = () => {
  return (
    <>
      <ul
        style={{
          display: "flex",
          justifyContent: "space-between",
          listStyleType: "none",
        }}
      >
        {items.map((item) => (
          <li key={item.id} style={{ margin: "0 1em" }}>
            <Link to={item.link}>{item.name}</Link>
          </li>
        ))}
      </ul>
    </>
  );
};

export { Navigation };

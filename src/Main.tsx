import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Sensors from "./pages/Sensors";
import Map from "./pages/Map";
import Reports from "./pages/Reports";
import { Header } from "./components/header/Header";
import "./styles/global/global.css";

function App() {
  return (
    <div className="App">
      <Header />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/sensors" element={<Sensors />} />
        <Route path="/map" element={<Map />} />
        <Route path="/reports" element={<Reports />} />
      </Routes>
    </div>
  );
}

export default App;

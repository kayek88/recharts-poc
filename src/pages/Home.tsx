import { useRef, useEffect, useState } from "react";
import Grid2 from "@mui/material/Unstable_Grid2";
import { Sidebar } from "../components/sidebar/Sidebar";
import {
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Line,
  ReferenceLine,
} from "recharts";

const data = [
  {
    x: 10,
    pv: 0,
  },
  {
    x: 20,
    pv: 30,
  },
  {
    x: 30,
    pv: 0,
  },
  {
    x: 35,
  },
  {
    x: 45,
    pv: 0,
  },
  {
    x: 50,
    pv: 130,
  },
  {
    x: 60,
    pv: 150,
  },
  {
    x: 75,
    pv: 130,
  },
  {
    x: 125,
    pv: 0,
  },
  {
    x: 160,
  },
  {
    x: 180,
    pv: 0,
  },
  {
    x: 220,
    pv: 130,
  },
  {
    x: 232,
    pv: 150,
  },
  {
    x: 261,
    pv: 130,
  },
  {
    x: 276,
    pv: 0,
  },
];

const Home = () => {
  const [ganttWidth, setGanttWidth] = useState("100%");
  const ref = useRef(null);

  const calculaPairs = (data: any, limit: number) => {
    let pairs: any = [];

    for (let index = 0; index < data.length; index++) {
      if (data[index].pv === limit) {
        pairs.push(data[index]);
      }
    }

    const chunkSize = 2;
    let chunks: any = [];

    for (let index = 0; index < pairs.length; index += chunkSize) {
      const chunk = pairs.slice(index, index + chunkSize);
      chunks.push(chunk);
    }

    return chunks;
  };

  const dataExceeded = calculaPairs(data, 130);
  const dataNormal = calculaPairs(data, 0);

  useEffect(() => {
    const el2 = ref.current;

    if (el2 !== null) {
      setGanttWidth(`${el2["props"]["width"]}px`);
    }
  }, [ganttWidth]);

  return (
    <main>
      <Grid2 container spacing={2} style={{ margin: "0 auto" }}>
        <Grid2 sm={2} xs={12}>
          <div>
            <Sidebar />
          </div>
        </Grid2>

        <Grid2 sm={8} xs={12}>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
              width: "100%",
              minHeight: "500px",
            }}
          >
            <LineChart
              width={730}
              height={250}
              data={data}
              margin={{ top: 5, right: 20, left: 0, bottom: 5 }}
            >
              <CartesianGrid stroke="#f2f2f2" strokeDasharray="5" ref={ref} />
              <XAxis
                dataKey="x"
                type={"number"}
                domain={[0, 300]}
                tickCount={20}
                allowDataOverflow={true}
              />
              <YAxis dataKey="200" domain={[0, 200]} type={"number"} />
              <Tooltip />
              <Legend />
              <Line type="linear" dataKey="pv" stroke="#0a6338" />
              <ReferenceLine y={130} stroke="red" strokeDasharray="3 0" />
            </LineChart>

            <div
              style={{
                width: "730px",
                margin: "50px auto 20px",
              }}
            >
              <div
                style={{
                  position: "relative",
                  flex: "none",
                  display: "block",
                  width: `${ganttWidth}`,
                  height: "10px",
                  marginLeft: "auto",
                  marginRight: "20px",
                }}
              >
                {dataNormal.map((item: any, index: number) => {
                  return (
                    <div
                      key={index}
                      style={{
                        position: "absolute",
                        top: 0,
                        left: `${(item[0].x / 300) * 100}%`,
                        right: `${100 - (item[1].x / 300) * 100}%`,
                        zIndex: 1,
                        height: "100%",
                        width: "auto",
                        backgroundColor: "#bebebe",
                        transition: "left 1s linear .15s, right 1s linear .15s",
                      }}
                    ></div>
                  );
                })}

                {dataExceeded.map((item: any, index: number) => {
                  return (
                    <div
                      key={index}
                      style={{
                        position: "absolute",
                        top: 0,
                        left: `${(item[0].x / 300) * 100}%`,
                        right: `${100 - (item[1].x / 300) * 100}%`,
                        zIndex: 2,
                        height: "100%",
                        width: "auto",
                        backgroundColor: "red",
                        transition: "left 1s linear .15s, right 1s linear .15s",
                      }}
                    ></div>
                  );
                })}
              </div>
            </div>
          </div>
        </Grid2>
      </Grid2>
    </main>
  );
};

export default Home;
